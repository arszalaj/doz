package com.salatsoft.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.client.TestRestTemplate.HttpClientOption;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FileControllerTest {
    @LocalServerPort
    private int port;

    private final TestRestTemplate restTemplate = new TestRestTemplate(HttpClientOption.ENABLE_COOKIES);

    @Test
    public void testGetFilesWithoutFilename() {
        ResponseEntity<String> response = restTemplate.getForEntity(getUrl(), String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        // Test other assertions
    }

    @Test
    public void testGetFilesWithFilename() {
        ResponseEntity<String> response = restTemplate.getForEntity(getUrl() + "?filename=test", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        // Test other assertions
    }

    private String getUrl() {
        return "http://localhost:" + port + "/files";
    }
}
