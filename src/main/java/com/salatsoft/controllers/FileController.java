package com.salatsoft.controllers;

import com.microsoft.graph.auth.confidentialClient.ClientCredentialProvider;
import com.microsoft.graph.auth.enums.NationalCloud;
import com.microsoft.graph.auth.publicClient.UsernamePasswordProvider;
import com.microsoft.graph.models.extensions.DriveItem;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.IDriveItemCollectionPage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FileController {
    @Value("${microsoft.graph.clientId}")
    private String clientId;

    @Value("${microsoft.graph.clientSecret}")
    private String clientSecret;

    @Value("${microsoft.graph.username}")
    private String username;

    @Value("${microsoft.graph.password}")
    private String password;

    @GetMapping("/files")
    public ResponseEntity<List<String>> getFiles(@RequestParam(required = false) String filename) {
        try {
            GraphServiceClient<Request> graphClient = createGraphClient();
            IDriveItemCollectionPage driveItems;

            if (filename != null) {
                driveItems = graphClient.me().drive().root().search(filename).buildRequest().get();
            } else {
                driveItems = graphClient.me().drive().root().children().buildRequest().get();
            }

            List<String> fileNames = new ArrayList<>();
            for (DriveItem item : driveItems.getCurrentPage()) {
                fileNames.add(item.name);
            }

            return ResponseEntity.ok(fileNames);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private GraphServiceClient<Request> createGraphClient() {
        if (username != null && password != null) {
            return GraphServiceClient.builder()
                    .authenticationProvider(new UsernamePasswordProvider(clientId, username, password))
                    .buildClient();
        } else {
            return GraphServiceClient.builder()
                    .authenticationProvider(new ClientCredentialProvider(NationalCloud.Global, clientId, clientSecret))
                    .buildClient();
        }
    }
}
